use ctrlc;
use env_logger;
use log::{error, info};
use std::env;
use std::fs::File;
use std::fs::OpenOptions;
use std::io::BufWriter;
use std::io::Write;
use std::ptr::{null_mut, read};
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;
use std::thread;
use std::{
    fmt,
    mem::size_of,
    time::{Duration, SystemTime, UNIX_EPOCH},
};
use winapi::{
    shared::{
        minwindef::{HIWORD, LOWORD},
        ntdef::NULL,
    },
    um::{
        handleapi::{CloseHandle, INVALID_HANDLE_VALUE},
        memoryapi::{CreateFileMappingW, MapViewOfFile, UnmapViewOfFile, FILE_MAP_READ},
        winnt::{HANDLE, PAGE_READONLY},
    },
};

fn format_utf16_string(utf16_string: &[u16]) -> String {
    String::from_utf16_lossy(utf16_string)
        .trim_end_matches("\u{0}")
        .to_string()
}

#[repr(C)]
struct Link {
    ui_version: u32,
    ui_tick: u32,
    f_avatar_position: [f32; 3],
    f_avatar_front: [f32; 3],
    f_avatar_top: [f32; 3],
    name: [u16; 256],
    f_camera_position: [f32; 3],
    f_camera_front: [f32; 3],
    f_camera_top: [f32; 3],
    identity: [u16; 256],
    context_len: u32,
}

impl fmt::Debug for Link {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "UI Version: {:?}", self.ui_version)?;
        write!(f, ", UI Tick: {:?}", self.ui_tick)?;
        write!(
            f,
            ", \r\nAvatar Position (Map): {:?}",
            self.f_avatar_position
        )?;
        write!(f, ", Avatar Front (Map): {:?}", self.f_avatar_front)?;
        write!(f, ", Avatar Top (Map): {:?}", self.f_avatar_top)?;
        write!(f, ", \r\nname: {:?}", format_utf16_string(&self.name))?;
        write!(f, ", \r\nCamera Position: {:?}", self.f_camera_position)?;
        write!(f, ", Camera Front: {:?}", self.f_camera_front)?;
        write!(f, ", Camera Top: {:?}", self.f_camera_top)?;
        write!(
            f,
            ", \r\nIdentity: {:?}",
            format_utf16_string(&self.identity)
        )?;
        write!(f, ", Context Length: {:?}", &self.context_len)
    }
}

#[repr(C)]
struct Context {
    server_address: [u8; 28],
    map_id: u32,
    map_type: u32,
    shard_id: u32,
    instance: u32,
    build_id: u32,
    ui_state: u32,
    compass_width: u16,
    compass_height: u16,
    compass_rotation: f32,
    player_x: f32,
    player_y: f32,
    map_center_x: f32,
    map_center_y: f32,
    map_scale: f32,
    process_id: u32,
    mount_index: u8,
}

impl fmt::Debug for Context {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Server Address: {:?}", self.server_address)?;
        write!(
            f,
            "\r\nMap ID: {:?}, Map Type: {:?}",
            self.map_id, self.map_type
        )?;
        write!(
            f,
            " | Shard ID: {:?}, Instance: {:?}",
            self.shard_id, self.instance
        )?;
        write!(f, "\r\nBuild ID: {:?}", self.build_id)?;
        write!(f, "\r\nUI State: {:?}", self.ui_state)?;
        write!(
            f,
            "\r\nCompass Width, Height: {:?}, {:?}",
            self.compass_width, self.compass_height
        )?;
        write!(f, " | Compass Rotation: {:?}", self.compass_rotation)?;
        write!(
            f,
            "\r\nPlayer X, Y: {:?}, {:?}",
            self.player_x, self.player_y
        )?;
        write!(
            f,
            "\r\nMap Center X, Y: {:?}, {:?}",
            self.map_center_x, self.map_center_y
        )?;
        write!(f, " | Map Scale: {:?}", self.map_scale)?;
        write!(f, "\r\nPID: {:?}", self.process_id)?;
        write!(f, "\r\nMount Index: {:?}", self.mount_index)
    }
}

#[repr(C)]
#[derive(Debug)]
struct Description {
    description: [u16; 2048],
}

#[repr(C)]
#[derive(Debug)]
struct MumbleLink {
    link: Link,
    context: Context,
    description: Description,
}

pub struct MapData {
    handle: HANDLE,
    pub id: String,
    pub map_size: usize,
    pub map_ptr: *mut u8,
}

impl Drop for MapData {
    fn drop(&mut self) {
        // Unmap memory from our process
        if self.map_ptr as *mut _ != NULL {
            unsafe {
                UnmapViewOfFile(self.map_ptr as *mut _);
            }
        }

        // Close our mapping
        if self.handle as *mut _ != NULL {
            unsafe {
                CloseHandle(self.handle);
            }
        }
    }
}

fn create_mapping(id: &str, map_size: usize) -> Result<MapData, std::io::Error> {
    let mut new_map: MapData = MapData {
        id: String::from(id),
        handle: NULL,
        map_size,
        map_ptr: null_mut(),
    };

    new_map.handle = unsafe {
        let unique_id: Vec<u16> = id.encode_utf16().collect();
        CreateFileMappingW(
            INVALID_HANDLE_VALUE,
            null_mut(),
            PAGE_READONLY,
            HIWORD(map_size as u32).into(),
            LOWORD(map_size as u32).into(),
            unique_id.as_ptr(),
        )
    };
    // Map our memory into our process
    new_map.map_ptr = unsafe { MapViewOfFile(new_map.handle, FILE_MAP_READ, 0, 0, 0) } as _;

    if new_map.handle == NULL || new_map.map_ptr.is_null() {
        return Err(std::io::Error::new(
            std::io::ErrorKind::Other,
            "Failed to create memory mapping",
        ));
    }
    Ok(new_map)
}

fn open_log_file(file_path: &str) -> std::io::Result<BufWriter<File>> {
    let file = OpenOptions::new()
        .create(true)
        .append(true)
        .open(file_path)?;
    Ok(BufWriter::new(file))
}

fn update_data(
    sleep_time: Duration,
    mem_file: &MapData,
    file_writer: &mut BufWriter<File>,
    running: Arc<AtomicBool>,
) -> std::io::Result<()> {
    // Read data from memory
    let mut data: MumbleLink = unsafe { read(mem_file.map_ptr as *const _) };
    // Save the player's last coordinates
    let mut player_coords = (data.context.player_x, data.context.player_y);

    while running.load(Ordering::SeqCst) {
        thread::sleep(sleep_time);

        // Read data from memory
        data = unsafe { read(mem_file.map_ptr as *const _) };
        // Save the player's last coordinates
        let last_player_coords = player_coords;
        // Get the player's current coordinates
        player_coords = (data.context.player_x, data.context.player_y);
        // If the player hasn't moved, don't write to the file
        if player_coords == last_player_coords {
            continue;
        }
        // Write the player's coordinates to the file
        match SystemTime::now().duration_since(UNIX_EPOCH) {
            Ok(timestamp) => {
                file_writer.write_all(
                    format!(
                        "TS:{:?} X:{} Y:{}\n",
                        timestamp.as_millis(),
                        player_coords.0,
                        player_coords.1
                    )
                    .as_bytes(),
                )?;
                file_writer.flush()?;
            }
            Err(_) => {
                error!("SystemTime before UNIX EPOCH!");
            }
        }
    }
    Ok(())
}

fn main() -> std::io::Result<()> {
    // Get the file path from the command line arguments
    let args: Vec<String> = env::args().collect();
    let mut file_path = "log.txt";
    if args.len() < 2 {
        println!("Please provide a file path");
        return Ok(());
    }
    file_path = &args[1];

    let mut file_writer = open_log_file(file_path)?;

    env_logger::init();
    // Create an AtomicBool to signal when the program should stop
    let running = Arc::new(AtomicBool::new(true));
    // Clone the Arc for the Ctrl-C handler
    let r = running.clone();
    // Set up Ctrl-C handler
    ctrlc::set_handler(move || {
        // Set running to false to stop the program
        r.store(false, Ordering::SeqCst);
    })
    .expect("Error setting Ctrl-C handler");

    let sleep_time = Duration::from_millis(250);
    // Create a memory mapping for the MumbleLink data
    let mem_file = create_mapping("MumbleLink", size_of::<MumbleLink>())?;
    update_data(sleep_time, &mem_file, &mut file_writer, running)?;

    info!("Application terminated gracefully");
    Ok(())
}
